import os
import unittest
import vtk, qt, ctk, slicer
import logging

def comparePoints(points, structure,overlapping = True,subsampleFactor = 4):

    """
    Given any set of points and a structure, return those points which overlap/are within the structure
    (CAN REUSE THIS IN ALL PARTS)

    Inputs:
    points - a string containing the name of the file containing the target points

    structure - a string containing the name of the file containing the structure data

    overlapping - 'True' to return overlapping points (DEFAULT), or 'False' to return non-overlapping points

    subsampleFactor - an int value specifying the subsampling factor of the image (DEFAULT = 4)
    """
    structureNode = slicer.util.getNode(structure)
    [xmax,ymax,zmax] = structureNode.GetImageData().GetDimensions()
    goodPoints = []
    
    if isinstance(points,str):
        pointsNode = slicer.util.getNode(points)
            
        for x in range(0,pointsNode.GetNumberOfMarkups()):
          world = [0,0,0,0]
          pointsNode.GetNthFiducialWorldCoordinates(x,world)
          imageVoxelID = structureNode.GetImageData().FindPoint(world[0]/subsampleFactor,world[1]/subsampleFactor,world[2]/subsampleFactor)
          
          xIndex = int(imageVoxelID%xmax)
          yIndex = int((imageVoxelID/xmax)%ymax)
          zIndex = int((imageVoxelID/(xmax*ymax))%zmax)
          
          imageVal = structureNode.GetImageData().GetScalarComponentAsDouble(xIndex,yIndex,zIndex,0)

          if overlapping == False:
              if imageVal == 0.0:
                  goodPoints.append(world)
          else:
              if imageVal != 0.0:
                  goodPoints.append(world)
              
    elif isinstance(points,list):
        for i in range(len(points)):

          imageVoxelID = structureNode.GetImageData().FindPoint(points[i][0]/subsampleFactor,points[i][1]/subsampleFactor,points[i][2]/subsampleFactor)

          xIndex = int(imageVoxelID%xmax)
          yIndex = int((imageVoxelID/xmax)%ymax)
          zIndex = int((imageVoxelID/(xmax*ymax))%zmax)
          
          imageVal = structureNode.GetImageData().GetScalarComponentAsDouble(xIndex,yIndex,zIndex,0)

          if overlapping == False:
              if imageVal == 0.0:
                  goodPoints.append(points[i])
          else:
              if imageVal != 0.0:
                  goodPoints.append(points[i])

    return goodPoints
