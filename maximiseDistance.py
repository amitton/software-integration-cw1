import os
import unittest
import numpy as np
import math
import vtk, qt, ctk, slicer
import logging
import sitkUtils as su
import SimpleITK as sitk

class ComputeDistanceImageFromLabelMap():
    
  def Execute(self, inputVolume):
    sitkInput = su.PullVolumeFromSlicer(inputVolume)
    distanceFilter = sitk.DanielssonDistanceMapImageFilter()
    
    # you can try other filters from here:
    # https://itk.org/Doxygen/html/group__ITKDistanceMap.html

    sitkOutput = distanceFilter.Execute(sitkInput)

    outputVolume = su.PushVolumeToSlicer(sitkOutput, None, 'distanceMap')

    return outputVolume
