import os
import unittest
import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
import logging
from TargetHippo import TargetHippoLogic
from TargetAvoidVentricles import TargetAvoidVentriclesLogic
from TargetAvoidVessels import TargetAvoidVesselsLogic
from comparePoints import comparePoints
from getLine import getLine
from lengthThreshold import lengthThreshold
from maximiseDistance import ComputeDistanceImageFromLabelMap

#
# CalcTrajectory
#

class CalcTrajectory(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "CalcTrajectory" # TODO make this more human readable by adding spaces
    self.parent.categories = ["Examples"]
    self.parent.dependencies = []
    self.parent.contributors = ["John Doe (AnyWare Corp.)"] # replace with "Firstname Lastname (Organization)"
    self.parent.helpText = """
This is an example of scripted loadable module bundled in an extension.
It performs a simple thresholding on the input volume and optionally captures a screenshot.
"""
    self.parent.helpText += self.getDefaultModuleDocumentationLink()
    self.parent.acknowledgementText = """
This file was originally developed by Jean-Christophe Fillion-Robin, Kitware Inc.
and Steve Pieper, Isomics, Inc. and was partially funded by NIH grant 3P41RR013218-12S1.
""" # replace with organization, grant and thanks.

#
# CalcTrajectoryWidget
#

class CalcTrajectoryWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setup(self):
    ScriptedLoadableModuleWidget.setup(self)

    # Instantiate and connect widgets ...

    #
    # Parameters Area
    #
    parametersCollapsibleButton = ctk.ctkCollapsibleButton()
    parametersCollapsibleButton.text = "Parameters"
    self.layout.addWidget(parametersCollapsibleButton)

    # Layout within the dummy collapsible button
    parametersFormLayout = qt.QFormLayout(parametersCollapsibleButton)

    #
    # input volume selector
    #
    self.inputSelector = slicer.qMRMLNodeComboBox()
    self.inputSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.inputSelector.selectNodeUponCreation = True
    self.inputSelector.addEnabled = False
    self.inputSelector.removeEnabled = False
    self.inputSelector.noneEnabled = False
    self.inputSelector.showHidden = False
    self.inputSelector.showChildNodeTypes = False
    self.inputSelector.setMRMLScene( slicer.mrmlScene )
    self.inputSelector.setToolTip( "Pick the input to the algorithm." )
    parametersFormLayout.addRow("Input Volume: ", self.inputSelector)

    #
    # output volume selector
    #
    self.outputSelector = slicer.qMRMLNodeComboBox()
    self.outputSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.outputSelector.selectNodeUponCreation = True
    self.outputSelector.addEnabled = True
    self.outputSelector.removeEnabled = True
    self.outputSelector.noneEnabled = True
    self.outputSelector.showHidden = False
    self.outputSelector.showChildNodeTypes = False
    self.outputSelector.setMRMLScene( slicer.mrmlScene )
    self.outputSelector.setToolTip( "Pick the output to the algorithm." )
    parametersFormLayout.addRow("Output Volume: ", self.outputSelector)

    #
    # Apply Button
    #
    self.applyButton = qt.QPushButton("Apply")
    self.applyButton.toolTip = "Run the algorithm."
    self.applyButton.enabled = False
    parametersFormLayout.addRow(self.applyButton)

    # connections
    self.applyButton.connect('clicked(bool)', self.onApplyButton)
    self.inputSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelect)
    self.outputSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelect)

    # Add vertical spacer
    self.layout.addStretch(1)

    # Refresh Apply button state
    self.onSelect()

  def cleanup(self):
    pass

  def onSelect(self):
    self.applyButton.enabled = self.inputSelector.currentNode() and self.outputSelector.currentNode()

  def onApplyButton(self):
    logic = CalcTrajectoryLogic()
    logic.run(self.inputSelector.currentNode(), self.outputSelector.currentNode())

#
# CalcTrajectoryLogic
#

class CalcTrajectoryLogic(ScriptedLoadableModuleLogic):
  """This class should implement all the actual
  computation done by your module.  The interface
  should be such that other python code can import
  this class and make use of the functionality without
  requiring an instance of the Widget.
  Uses ScriptedLoadableModuleLogic base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def hasImageData(self,volumeNode):
    """This is an example logic method that
    returns true if the passed in volume
    node has valid image data
    """
    if not volumeNode:
      logging.debug('hasImageData failed: no volume node')
      return False
    if volumeNode.GetImageData() is None:
      logging.debug('hasImageData failed: no image data in volume node')
      return False
    return True

  def isValidInputOutputData(self, inputVolumeNode, outputVolumeNode):
    """Validates if the output is not the same as input
    """
    if not inputVolumeNode:
      logging.debug('isValidInputOutputData failed: no input volume node defined')
      return False
    if not outputVolumeNode:
      logging.debug('isValidInputOutputData failed: no output volume node defined')
      return False
    if inputVolumeNode.GetID()==outputVolumeNode.GetID():
      logging.debug('isValidInputOutputData failed: input and output volume is the same. Create a new volume for output to avoid this error.')
      return False
    return True

  def run(self, inputVolume, outputVolume):
    """
    Run the actual algorithm
    """

    if not self.isValidInputOutputData(inputVolume, outputVolume):
      slicer.util.errorDisplay('Input volume is the same as output volume. Choose a different output volume.')
      return False

    logging.info('CalcTrajectory - Processing started')

    entries = slicer.util.getNode('entries')
    targets = slicer.util.getNode('targets')
    
    print('Total number of trajectories = ', entries.GetNumberOfMarkups()*targets.GetNumberOfMarkups())
    
    hippoLogic = TargetHippoLogic()
    vesselsLogic = TargetAvoidVesselsLogic()
    ventriclesLogic = TargetAvoidVentriclesLogic()
    
    # STEP ONE - Target the hippocampus
    targetsToKeep = hippoLogic.run(inputVolume,outputVolume,'r_hippoTest')

    # STEP TWO - Avoid the vessels
    trajToKeepFirst = vesselsLogic.run(inputVolume,outputVolume,'vesselsTestDilate1',differentTargets = targetsToKeep)

    # STEP THREE - Avoid the ventricles
    trajToKeepSecond = ventriclesLogic.run(inputVolume,outputVolume,'ventriclesTest',differentTrajectories = trajToKeepFirst)
      
    # STEP FOUR - Perform length thresholding
    print('Length Thresholding - Processing started')
    trajToKeepThird = lengthThreshold(trajToKeepSecond)
    print('Length Thresholding - Processing completed')
    
    # STEP FIVE - Pick optimum trajectory
    distanceFilter = ComputeDistanceImageFromLabelMap()
    distanceMap = distanceFilter.Execute('vesselsTestDilate1')
    distanceMapNode = slicer.util.getNode('distanceMap')
    
    subsampleFactor = 4
    [xmax,ymax,zmax] = distanceMapNode.GetImageData().GetDimensions()
    
    maxDistance = 0
    bestTraj = []
    
    for traj in trajToKeepThird:
      
      distance = 0
      
      for point in traj:
        distanceValueVoxelID = distanceMapNode.GetImageData().FindPoint(point[0]/subsampleFactor,point[1]/subsampleFactor,point[2]/subsampleFactor)
         
        xIndex = int(distanceValueVoxelID%xmax)
        yIndex = int((distanceValueVoxelID/xmax)%ymax)
        zIndex = int((distanceValueVoxelID/(xmax*ymax))%zmax)
        
        distanceVal = distanceMapNode.GetImageData().GetScalarComponentAsDouble(xIndex,yIndex,zIndex,0)
        distance += distanceVal

      if distance > maxDistance:
        maxDistance = distance
        bestTraj = traj

    print('Optimum Trajectory:')
    print(' - Entry point =', bestTraj[0])
    print(' - Target point =', bestTraj[-1])
    print(' - Distance from vessels =', maxDistance)
    
    logging.info('CalcTrajectory - Processing completed')

    return bestTraj


class CalcTrajectoryTest(ScriptedLoadableModuleTest):
  """
  This is the test case for your scripted module.
  Uses ScriptedLoadableModuleTest base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setUp(self):
    """ Do whatever is needed to reset the state - typically a scene clear will be enough.
    """
    slicer.mrmlScene.Clear(0)
    import SampleData
    SampleData.downloadFromURL(
      nodeNames='FA',
      fileNames='FA.nrrd',
      uris='http://slicer.kitware.com/midas3/download?items=5767')
    self.delayDisplay('Finished with download and loading')

    volumeNode = slicer.util.getNode(pattern="FA")
    
  
  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    #self.test_CalcTrajectory1()
    self.test_CalcTrajectoryComparePoints1()
    self.test_CalcTrajectoryComparePoints2()
    self.test_CalcTrajectoryComparePoints3()
    self.test_CalcTrajectoryGetLine1()
    self.test_CalcTrajectoryGetLine2()
    self.test_CalcTrajectoryLengthThreshold1()
    
  def test_CalcTrajectory1(self):
    """ Ideally you should have several levels of tests.  At the lowest level
    tests should exercise the functionality of the logic with different inputs
    (both valid and invalid).  At higher levels your tests should emulate the
    way the user would interact with your code and confirm that it still works
    the way you intended.
    One of the most important features of the tests is that it should alert other
    developers when their changes will have an impact on the behavior of your
    module.  For example, if a developer removes a feature that you depend on,
    your test should break so they know that the feature is needed.
    """

    self.delayDisplay("Starting the test")
    #
    # first, get some data
    #
    import SampleData
    SampleData.downloadFromURL(
      nodeNames='FA',
      fileNames='FA.nrrd',
      uris='http://slicer.kitware.com/midas3/download?items=5767')
    self.delayDisplay('Finished with download and loading')

    volumeNode = slicer.util.getNode(pattern="FA")
    logic = CalcTrajectoryLogic()
    self.assertIsNotNone( logic.hasImageData(volumeNode) )
    self.delayDisplay('Test passed!')

  def test_CalcTrajectoryComparePoints1(self):
    """ A test to ensure a type error is raised if compare points is given a complex argument
    """
    self.delayDisplay("Starting the test - ComparePoints1")
    testPoints = [[1j,1j,1j]]

    with self.assertRaises(TypeError):
      comparePoints(testPoints,'FA')

    self.delayDisplay('ComparePoints1 - Test passed!')
    
  def test_CalcTrajectoryComparePoints2(self):
    """ A test to ensure a known point in the structure is correctly identified as overlapping
        with the structure
    """
    self.delayDisplay("Starting the test - ComparePoints2")
    testPoints = [[131,130,25]]
    
    testPointsToKeep = comparePoints(testPoints,'FA',subsampleFactor = 1)
    self.assertNotEqual(testPointsToKeep,[])

    self.delayDisplay('ComparePoints2 - Test passed!')

  def test_CalcTrajectoryComparePoints3(self):
    """ A test to ensure the "overlapping = False" feature works, i.e. a target can be correctly
        avoided
    """
    self.delayDisplay("Starting the test - ComparePoints3")
    testPoints = [[131,130,25]]

    testPointsToKeep = comparePoints(testPoints,'FA',overlapping = False, subsampleFactor = 1)
    self.assertEqual(testPointsToKeep,[])

    self.delayDisplay('ComparePoints3 - Test passed!')

  def test_CalcTrajectoryGetLine1(self):
    """ A test to ensure a value error is raised when getLine is called on the same point twice
    """
    self.delayDisplay("Starting the test - GetLine1")
    testPoint1 = [1,1,1]
    testPoint2 = [1,1,1]

    with self.assertRaises(ValueError):
      getLine(testPoint1,testPoint2,10)

    self.delayDisplay('GetLine1 - Test passed!')

  def test_CalcTrajectoryGetLine2(self):
    """ A test to ensure a list of points is returned from a call to getLine
    """
    self.delayDisplay("Starting the test - GetLine2")
    testPoint1 = [1,1,1]
    testPoint2 = [10,10,10]

    self.assertTrue(isinstance(getLine(testPoint1,testPoint2,10),list))

    self.delayDisplay('GetLine2 - Test passed!')

  def test_CalcTrajectoryLengthThreshold1(self):
    """ A test to ensure length thresholding correctly removes trajectories that are too long
    """
    self.delayDisplay("Starting the test - LengthThreshold1")
    testPoint1 = [1,1,1]
    testPoint2 = [10,10,10]
    
    traj = getLine(testPoint1,testPoint2,10)
    goodTrajectories = lengthThreshold([traj],1)

    self.assertEqual(goodTrajectories,[])

    self.delayDisplay('LengthThreshold1 - Test passed!')
