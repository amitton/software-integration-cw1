import os
import unittest
import numpy as np
import math
import vtk, qt, ctk, slicer
import logging

def lengthThreshold(trajectories,threshold = None):

    goodTrajectories = []
    trajLengths = []

    if threshold != None:

        for traj in trajectories:

            entryPoint = traj[0]
            endPoint= traj[-1]

            trajLength = math.sqrt(((endPoint[0]-entryPoint[0])**2)
                                   +((endPoint[1]-entryPoint[1])**2)
                                   +((endPoint[2]-entryPoint[2])**2))

            if trajLength < threshold:

                goodTrajectories.append(traj)

    else:
        
        for traj in trajectories:

            entryPoint = traj[0]
            endPoint= traj[-1]

            trajLength = math.sqrt(((endPoint[0]-entryPoint[0])**2)
                                   +((endPoint[1]-entryPoint[1])**2)
                                   +((endPoint[2]-entryPoint[2])**2))

            trajLengths.append(trajLength)

        meanLength = np.mean(trajLengths)
        stdLength = np.std(trajLengths)

        print('Mean length = ', meanLength, 'STD length = ', stdLength)

        goodTrajectories = lengthThreshold(trajectories,threshold = meanLength+1*stdLength)
        

    print('Number of good trajectories = ', len(goodTrajectories))

    return goodTrajectories
