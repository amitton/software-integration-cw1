import os
import unittest
import numpy as np
import vtk, qt, ctk, slicer
import logging

def getLine(point1,point2,num):

    """
    A function to return a line made up of num evenly spaced points, given a start and end point

    points 1 and 2 are in the form [x,y,z,1.0] (result of GetWorldCoords from Fiducial markers)
    """
    if point1 == point2:
        raise ValueError('The same point passed twice - cannot create line!')
    points = []

    xPoints = np.linspace(point1[0],point2[0],num=num)
    yPoints = np.linspace(point1[1],point2[1],num=num)
    zPoints = np.linspace(point1[2],point2[2],num=num)

    for i in range(0,num):
        points.append([xPoints[i],yPoints[i],zPoints[i]])

    return points
